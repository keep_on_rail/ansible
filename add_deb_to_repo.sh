#!/bin/bash
red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`
USERNAME=root
HOST=192.168.14.191
DIR=/home

read -p "${green}Place your deb packages into $DIR directory and press [ENTER]: "

echo "Adding your deb package to local repository ${reset}"

sftp $HOST << !
put $DIR/*.deb $DIR/
bye
!

ssh $USERNAME@$HOST << ! 
echo "${green}Add your deb packages to repository.${reset}"
reprepro -P 1 -b /var/repositories includedeb stretch $DIR/*.deb
rm $DIR/*.deb
echo "${green}Check your package${reset}"
reprepro -b /var/repositories list stretch
exit
!

$reset

echo "Should I ${red}remove${reset} your local deb packages from $DIR? y/n?"
read CLEAR_CACHE

if [[ $CLEAR_CACHE = "y" ]]; then
	echo "Deleting ..."
	rm $DIR/*.deb
	echo "Done, Bye!"
else
	echo "Bye!"
fi
$reset